line1 = list(map(lambda x: int(x), input().split()))

numBuildings = line1[0]
numPipes = line1[1]
enhancerStrength = line1[2]

"""
each row of this 2D list is a pipe
a Pipe is 3-tuple stating a building, another building, and the cost of connecting the two
"""
connections = []
for i in range(0, numPipes):
    connections.append(list(map(lambda x: int(x), input().split())))

currentPlan = connections[0 : numBuildings - 1]

"""
construct a tree that connects all buildings as efficiently as possible
We use Prim's algorithm:
    1. Start with building 1 as our tree
    2. Find the unconnected node that has the cheapest available connection to our tree, and connect it
    3. Repeat step 2 until all nodes are connected
returns: [pipe]
    the finished tree
"""
def buildTree():
    """
    find the cheapest pipe that connects a building not in the tree to the buildings in the tree
    params: [int]
        the buildings in the tree so far
    returns: Pipe
    """
    def findNextNode(buildingsInTree):
        #find all pipes that connect the tree to an unconnected building
        pipes = filter(lambda p: (p[0] in buildingsInTree and p[1] not in buildingsInTree) or
                                 (p[1] in buildingsInTree and p[0] not in buildingsInTree), connections)
        #get the one with lowest cost
        return min(pipes, key=(lambda x: x[2]))
    """
    the intermediate step tracks the pipes that make up our tree so far as well as the buildings connected so far
    this prevents the function from going in circles
    params: [Pipe] [int]
        the pipes we have not examined
        the tree so far
        buildings in the tree so far
    """
    def buildTreeStep(tree, connectedBuildings):
        #build until every building is connected
        while len(connectedBuildings) != numBuildings:
            #find the pipe to the closest unconnected node
            nextPipe = findNextNode(connectedBuildings)
            tree.append(nextPipe)
            if nextPipe[0] not in connectedBuildings:
                connectedBuildings.append(nextPipe[0])
            else:
                connectedBuildings.append(nextPipe[1])
        return tree
    return buildTreeStep([], [1])

bestPlan = buildTree()
"""
find number of days needed to change to the new plan
this is the maximum between number of pipes we need to add and pipes we need to remove
"""
print(max(len(list(filter(lambda x: x not in currentPlan, bestPlan))),
          len(list(filter(lambda x: x not in bestPlan, currentPlan)))))