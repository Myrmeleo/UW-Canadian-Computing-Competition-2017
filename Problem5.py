"""
A station line will be represented as a linked list, with each station being a node
"""
class StationLine:
    """
    param: int
        the number of the station line
    """
    def __init__(self, number):
        self.number = number
        self.stationCount = 0
        self.lowest = None
        self.highest = None

    """
    the parameters of the problem ensure at any station we add will have the new highest number
    """
    def addStation(self, newStation):
        self.stationCount += 1
        if self.highest is not None:
            self.highest.next = newStation
            newStation.previous = self.highest
        self.highest = newStation
        if self.lowest is None:
            self.lowest = newStation

    """
    move each train to the next station on this line
    """
    def rotate(self):
        if self.highest is not None:
            #line is circular, so each station sends its people to the next one
            temp = self.highest.peopleCount
            iterator = self.highest
            while iterator.previous is not None:
                iterator.peopleCount = iterator.previous.peopleCount
                iterator = iterator.previous
            #highest station sends its people to the lowest station
            self.lowest.peopleCount = temp


class Station:
    """
    param: int
        the number of the station
        the number of people currently at this station
    """
    def __init__(self, number, count):
        self.number = number
        self.peopleCount = count
        self.previous = None
        self.next = None


line1 = list(map(lambda x: int(x), input().split()))

numStations = line1[0]
numLines = line1[1]
numActions = line1[2]

#the subway line numbers that each station belongs to
subLineNums = list(map(lambda x: int(x), input().split()))
#inital number of passengers for each station
startingPassengerCount = list(map(lambda x: int(x), input().split()))

#contruct stations and station lines
stationLines = []
for i in range(1, numLines + 1):
    stationLines.append(StationLine(i))
stations = []
for i in range(0, numStations):
    stations.append(Station(i + 1, startingPassengerCount[i]))

"""
easily retrieve a desired station or station line
param: int
    the number of the one we want
returns: (Station or StationLine)
"""

def getStationLine(i):
    return stationLines[i - 1]
def getStation(i):
        return stations[i - 1]

"""
get the number of passengers between stations numbered l and r
params: int int
    left and right bound (inclusive)
returns: int
"""
def sumPassengers(l, r):
    sum = 0
    for i in range(l, r + 1):
        sum += getStation(i).peopleCount
    return sum

#put each station in its assigned line
for i in range(0, numStations):
    getStationLine(subLineNums[i]).addStation(getStation(i + 1))

#execute the actions (surveys for operations)
for i in range(0, numActions):
    action = list(map(lambda x: int(x), input().split()))
    #survey
    if action[0] == 1:
        print(sumPassengers(action[1], action[2]))
    #rotate line
    else:
        getStationLine(action[1]).rotate()