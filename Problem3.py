numPlanks = int(input())

plankLengths = list(map(lambda x: int(x), input().split()))

#we find the length of the fence we can make for each height
exploredHeights = []
lengthForHeights = []

#examine every possible height of the fence, by looking at every combination of 2 planks
for i in range(0, numPlanks):
    for j in range(i + 1, numPlanks):
        #get the height of these two planks and check if we already examined this height
        currentHeight = plankLengths[i] + plankLengths[j]
        if currentHeight not in exploredHeights:
            exploredHeights.append(currentHeight)
            #find the length of the fence at this height
            #we will make a copy of the planks and remove each plank as it is added to the fence
            plankLengthsCopy = plankLengths.copy()
            lengthSoFar = 0
            while len(plankLengthsCopy) >= 2:
                #find a plank that fits the first in this list to make the required height
                plank1 = plankLengthsCopy[0]
                requiredPlank2 = currentHeight - plank1
                plankLengthsCopy.remove(plank1)
                if requiredPlank2 in plankLengthsCopy:
                    lengthSoFar += 1
                    plankLengthsCopy.remove(requiredPlank2)
            lengthForHeights.append(lengthSoFar)

print(str(max(lengthForHeights)) + " " + str(lengthForHeights.count(max(lengthForHeights))))