Leo Huang 2017 (updated with documentation 2020)

These are my solutions to the 2017 UW senior computing competition.  

I dug these files out of the hard drive on my old computer, then added documentation to all the code.  

The problem specifications are available at https://www.cemc.uwaterloo.ca/contests/computing/2017/stage%201/seniorEF.pdf  
