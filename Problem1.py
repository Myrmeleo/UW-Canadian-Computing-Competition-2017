numDays = int(input())

#highest day so far where the scores are the same
highestDay = 0

#for each team, get lists of scores for each day
swiftScores = list(map(lambda x: int(x), input().split()))
semaphoreScores = list(map(lambda x: int(x), input().split()))

#total scores
swiftTotal = 0
semaphoreTotal = 0

for i in range(0, numDays):
    swiftTotal += swiftScores[i]
    semaphoreTotal += semaphoreScores[i]
    if swiftTotal == semaphoreTotal:
        highestDay = i + 1

print(highestDay)