numRecords = int(input())

#recorded wave heights
records = list(map(lambda x: int(x), input().split()))

records.sort()

#partition records into low half and high half
lowRecords = sorted(records[0 : int((numRecords + 1) / 2)], reverse=True)
highRecords = records[int((numRecords + 1) / 2) : numRecords]

properOrderedRecords = []

#merge reverse of low half to high half, one from each at a time
for i in range(0, int((numRecords + 1) / 2)):
    properOrderedRecords.append(lowRecords[i])
    if i < len(highRecords):
        properOrderedRecords.append(highRecords[i])

print(*properOrderedRecords)